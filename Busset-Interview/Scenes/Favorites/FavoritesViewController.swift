//
//  FavoritesViewController.swift
//  Busset-Interview
//
//  Created by Ahmed Yasser on 31/08/2021.
//

import UIKit
import RxSwift
class FavoritesViewController: UIViewController {

    
    // MARK:- IBOutlets
    @IBOutlet private weak var tableView: UITableView!
    
    // MAKR:- Variables
    private var viewModel: FavoritesViewModelProtocol!
    private let disposeBag = DisposeBag()

    
    // MARK:- Init
    
    init(viewModel: FavoritesViewModelProtocol) {
        self.viewModel = viewModel
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTableView()
        bindTableView()

        // Do any additional setup after loading the view.
    }

    
    // MARK:- Functions
    
    func setupTableView() {
        let nib = UINib(nibName: "ImageTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "ImageTableViewCell")
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 200
    }

    
    func bindTableView() {
        viewModel.photosObservable.bind(to: tableView.rx.items) { tableView, index, item in
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ImageTableViewCell") as? ImageTableViewCell else { return UITableViewCell() }
            cell.photoSaved = item
            cell.favTap.subscribe { _ in
                if cell.heartButton.isSelected {
                    self.viewModel.unFav(index: index) {
                        cell.heartButton.isSelected.toggle()
                    }
                }
            }.disposed(by: cell.disposeBag)
            return cell
            
        }.disposed(by: disposeBag)
        
    }



}
