//
//  FavoritesViewModel.swift
//  Busset-Interview
//
//  Created by Ahmed Yasser on 31/08/2021.
//

import Foundation
import RxSwift
import RxCocoa

protocol FavoritesViewModelProtocol {
    var photosObservable: Observable<[FavPhoto]> {get}
    func unFav(index: Int, completion: () -> Void)
}

final class FavoritesViewModel {
    
    
    // MARK:- Variables
    private let photos = BehaviorRelay<[FavPhoto]>(value: [])
    lazy var photosObservable: Observable<[FavPhoto]> = self.photos.asObservable()

    
    
    // MARK:- Init
    
    init() {
        self.photos.accept(sharedRealm.read(FavPhoto.self).toArray())
    }
    
    
    
    
}

extension FavoritesViewModel: FavoritesViewModelProtocol {
    func unFav(index: Int, completion: () -> Void) {
        sharedRealm.delete(object: FavPhoto.self, predicate: NSPredicate(format: "id == %@", photos.value[index].id)) { [weak self] success in
            guard let self = self else {return}
            if success {
                // Should remove
                self.photos.remove(at: index)
                completion()
                
            } else {
                // Something Went Wrong
            }
        }
    }

}
