//
//  ImageTableViewCell.swift
//  Busset-Interview
//
//  Created by Ahmed Yasser on 29/08/2021.
//

import UIKit
import RxSwift

class ImageTableViewCell: UITableViewCell {

    // MARK:- IBOutlets
    
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var heartButton: UIButton!
    
    // MARK:- Variables
    var disposeBag = DisposeBag()
    var favTap : Observable<Void>{

        return self.heartButton.rx.tap.asObservable()

    }
    
    var photoSaved: FavPhoto! {
        willSet {
            disposeBag = DisposeBag()
        }
        
        didSet {
            titleLabel.text = photoSaved.title
            heartButton.isSelected = true
            guard let data = photoSaved.image else { return }
            photoImageView.image = UIImage(data: data)
        }
    }

    var photoItem: Photo! {
        willSet {
            disposeBag = DisposeBag()
        }
        
        didSet {
            titleLabel.text = photoItem.title
            heartButton.isSelected = sharedRealm.object(object: FavPhoto.self, primaryKey: photoItem.id) != nil
            guard let url = URL(string: photoItem.urlM ?? "") else { return }
            photoImageView.kf.setImage(with: url)
            photoImageView.kf.indicatorType = .activity

        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    // MARK:- IBActions
    
}
