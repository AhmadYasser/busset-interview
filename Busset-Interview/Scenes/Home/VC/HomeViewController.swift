//
//  HomeViewController.swift
//  Busset-Interview
//
//  Created by Ahmed Yasser on 28/08/2021.
//

import UIKit
import Kingfisher
import RxSwift

protocol HomeViewProtocol: PaginableView {
    
}

class HomeViewController: UIViewController {

    // MARK:- IBOutlets
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var searchBar: UISearchBar!
    
    // MARK:- Variables
    private var viewModel: HomeViewModelProtocol!
    private let disposeBag = DisposeBag()
    private var isViewFetching = false
    // MARK:- DataSource
    
    
    // MARK:- Init
    
    init(viewModel: HomeViewModelProtocol) {
        self.viewModel = viewModel
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTableView()
        bindTableView()
        bindSearchBar()
        addRightButton()
        // Do any additional setup after loading the view.
    }

    
    

    

    
    // MARK:- Functions
    
    func setupTableView() {
        let nib = UINib(nibName: "ImageTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "ImageTableViewCell")
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 200
    }
    
    
    func bindTableView() {
        viewModel.photosObservable.bind(to: tableView.rx.items) { tableView, index, item in
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ImageTableViewCell") as? ImageTableViewCell else { return UITableViewCell() }
            cell.photoItem = item
            cell.favTap.subscribe { _ in
                if cell.heartButton.isSelected {
                    self.viewModel.unFav(index: index) {
                        cell.heartButton.isSelected.toggle()
                    }
                } else {
                    guard let data = cell.photoImageView?.image?.jpegData(compressionQuality: 0.5) else { return  }
                    self.viewModel.fav(index: index, image: data) {
                        cell.heartButton.isSelected.toggle()
                    }
                }
            }.disposed(by: cell.disposeBag)
            return cell
            
        }.disposed(by: disposeBag)
        
        tableView.rx.didScroll.subscribe { [weak self] _ in
            guard let self = self else {return}
            self.didEndScrolling()
            
        }.disposed(by: disposeBag)
    }
    
    
    func bindSearchBar() {
        searchBar.rx.searchButtonClicked.subscribe(onNext: {[weak self] in
            guard let self = self else {return}
            guard let text = self.searchBar.text , !text.isEmpty else {return}
            self.viewModel.search(text: text)
            self.searchBar.endEditing(true)
        }).disposed(by: disposeBag)
    }
    
    
    func addRightButton() {
        let button = UIButton()
        button.setImage(UIImage(systemName: "heart.fill"), for: .normal)
        button.addTarget(self, action: #selector(goToFavorites), for: .touchUpInside)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button)
    }
    
    
    // MARK:- IBActions
    @IBAction private func goToFavorites() {
        let coordinator = FavoritesCoordinator(navigationController: self.navigationController ?? UINavigationController())
        coordinator.start()
    }



}


extension HomeViewController: HomeViewProtocol {
    var isFetching: Bool {
        return isViewFetching
    }
    
    func startFetching() {
        isViewFetching = true
    }
    
    func endFetching() {
        isViewFetching = false
    }
    
    var scroll: UIScrollView {
        return tableView
    }
    
    var paginablePresenter: PaginablePresenter {
        return viewModel
    }
    
    
}
