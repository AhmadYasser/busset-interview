//
//  HomeViewModel.swift
//  Busset-Interview
//
//  Created by Ahmed Yasser on 30/08/2021.
//

import Foundation
import RxSwift
import RxCocoa

protocol HomeViewModelProtocol: PaginablePresenter {
    var photosObservable: Observable<[Photo]> {get}
    func search(text: String)
    func fav(index: Int, image: Data, completion: () -> Void)
    func unFav(index: Int, completion: () -> Void)
}

final class HomeViewModel {
    
    // MARK:- Variables
    private let photos = BehaviorRelay<[Photo]>(value: [])
    lazy var photosObservable: Observable<[Photo]> = self.photos.asObservable()
    private var pages = 0
    private var thisPage = 0
    private var isViewModelFetching = false
    private var isSearching = false
    private var currentSearchString = ""
    init() {
        getRecent(page: 1)
    }
    
    
    // MARK:- APIStuff
    func getRecent(page: Int) {
        isFetching = true
        isSearching = false
        NetworkManager.getRecent(page: page) { [weak self] result in
            guard let self = self else {return}
            self.isFetching = false
            switch result {
            
            case .success(let model):
                if page == 1 {
                    self.photos.accept(model.photos.photo)
                } else {
                    self.photos.accept( self.photos.value + model.photos.photo)
                }
                self.pages = model.photos.pages
                self.thisPage = model.photos.page
            case .failure(let error):
                print(error)
                
            }
            
        }

    }

    
    func searchCall(text: String, page: Int) {
        isFetching = true
        NetworkManager.search(text: text, page: page) { [weak self] result in
            guard let self = self else {return}
            self.isFetching = false
            
            switch result {
            
            case .success(let model):
                if page == 1 {
                    self.photos.accept(model.photos.photo)
                } else {
                    self.photos.accept( self.photos.value + model.photos.photo)
                }
                self.pages = model.photos.pages
                self.thisPage = model.photos.page
            case .failure(let error):
                print(error)
                
            }

        }

    }

}

extension HomeViewModel: HomeViewModelProtocol {
    var isFetching: Bool {
        get {
            return isViewModelFetching
        }
        set {
            isViewModelFetching = newValue
        }
    }
    
    
    var currentPage: Int {
        return thisPage
    }
    
    var lastPage: Int {
        return pages
    }
    
    func paginate() {
        if isSearching {
            searchCall(text: currentSearchString, page: currentPage + 1)
        } else {
            getRecent(page: currentPage + 1)
        }
    }
    
    func search(text: String) {
        pages = 0
        thisPage = 0
        isSearching = true
        currentSearchString = text
        searchCall(text: text, page: 1)
        
    }
    
    func fav(index: Int, image: Data, completion: () -> Void) {
        let object = FavPhoto(id: photos.value[index].id, image: image, title: photos.value[index].title)
        sharedRealm.add(object)
        completion()
    }
    
    
    func unFav(index: Int, completion: () -> Void) {
        sharedRealm.delete(object: FavPhoto.self, predicate: NSPredicate(format: "id == %@", photos.value[index].id)) { [weak self] success in
            guard let self = self else {return}
            if success {
                // Should remove
                completion()
                
            } else {
                // Something Went Wrong
            }
        }
    }

}
