//
//  HomeCoordinator.swift
//  Busset-Interview
//
//  Created by Ahmed Yasser on 30/08/2021.
//

import UIKit

final class HomeCoordinator: Coordinator {
     
    func start() {
        let viewModel = HomeViewModel()
        let viewController = HomeViewController(viewModel: viewModel)
        navigationController?.pushViewController(viewController, animated: true)
    }
}
