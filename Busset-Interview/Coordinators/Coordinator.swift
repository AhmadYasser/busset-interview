//
//  Coordinator.swift
//  Busset-Interview
//
//  Created by Ahmed Yasser on 30/08/2021.
//

import UIKit

class Coordinator {
    
    var childViewControllers = [Coordinator]()
    weak var navigationController: UINavigationController?
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

}
