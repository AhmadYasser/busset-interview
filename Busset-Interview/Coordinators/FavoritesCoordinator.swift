//
//  FavoritesCoordinator.swift
//  Busset-Interview
//
//  Created by Ahmed Yasser on 31/08/2021.
//

import Foundation

class FavoritesCoordinator: Coordinator {
    
    func start() {
        let viewModel = FavoritesViewModel()
        let viewController = FavoritesViewController(viewModel: viewModel)
        navigationController?.pushViewController(viewController, animated: true)

    }
}
