//
//  AppDelegate.swift
//  Busset-Interview
//
//  Created by Ahmed Yasser on 28/08/2021.
//

import UIKit
import IQKeyboardManagerSwift
@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        window = UIWindow()
        window?.backgroundColor = .white
        let navigationController = UINavigationController()
        window?.rootViewController = navigationController
        let coordinator = HomeCoordinator(navigationController: navigationController)
        coordinator.start()
        window?.makeKeyAndVisible()

        
        
        IQKeyboardManager.shared.enable = true
        return true
    }

}

