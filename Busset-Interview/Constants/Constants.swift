//
//  Constants.swift
//  Busset-Interview
//
//  Created by Ahmed Yasser on 28/08/2021.
//

import Foundation


struct FlickrURL {
    static let APIURL = "https://api.flickr.com/"
}


struct FlickrAPIKeys {
    static let Method = "method"
    static let APIKey = "api_key"
    static let Extras = "extras"
    static let ResponseFormat = "format"
    static let DisableJSONCallback = "nojsoncallback"
    static let SafeSearch = "safe_search"
    static let Text = "text"
    static let Page = "page"
    static let PerPage = "per_page"
    static let PhotoID = "photo_id"
}

struct FlickrAPIValues {
    static let RecentMethod = "flickr.photos.getRecent"
    static let SearchMethod = "flickr.photos.search"
    static let AddToFavMethod = "flickr.favorites.add"
    static let RemoveFavMethod = "flickr.favorites.remove"
    static let APIKey = "ed6f3bf48c8790a6f70a14b638046f4f"
    static let ResponseFormat = "json"
    static let DisableJSONCallback = "1"
    static let MediumURL = "url_m"
    static let SafeSearch = "1"
    static let PerPage = "20"
}



