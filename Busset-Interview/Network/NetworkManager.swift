//
//  NetworkManager.swift
//  Busset-Interview
//
//  Created by Ahmed Yasser on 28/08/2021.
//

import Foundation
import Alamofire
class NetworkManager {
    class func getRecent(page: Int, completion: @escaping (Result<PhotosBaseModelData, Error>) -> Void) {
        AF.request(PhotosRoute.getRecent(page)) .validate() .responseData { response in
            switch response.result {
            
            case .success(let value):
                do {
                    let model = try JSONDecoder().decode(PhotosBaseModelData.self, from: value)
                    completion(.success(model))
                } catch {
                    completion(.failure(error))
                }
                
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    
    class func search(text: String, page: Int, completion: @escaping (Result<PhotosBaseModelData, Error>) -> Void) {
        AF.request(PhotosRoute.search(page: page, text: text)) .validate() .responseData { response in
            switch response.result {
            
            case .success(let value):
                do {
                    let model = try JSONDecoder().decode(PhotosBaseModelData.self, from: value)
                    completion(.success(model))
                } catch {
                    completion(.failure(error))
                }
                
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
