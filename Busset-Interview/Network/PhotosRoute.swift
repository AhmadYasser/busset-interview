//
//  NetworkRoute.swift
//  Busset-Interview
//
//  Created by Ahmed Yasser on 28/08/2021.
//

import Foundation
import Alamofire


enum PhotosRoute: URLRequestConvertible {
    
    case getRecent(Int)
    case search(page: Int, text: String)
    
    var method: HTTPMethod {
        return .get
    }
    
    var parameters: [String : Any]? {
        var params = [FlickrAPIKeys.APIKey: FlickrAPIValues.APIKey,
                      FlickrAPIKeys.Extras: FlickrAPIValues.MediumURL,
                      FlickrAPIKeys.ResponseFormat: FlickrAPIValues.ResponseFormat,
                      FlickrAPIKeys.DisableJSONCallback:FlickrAPIValues.DisableJSONCallback,
                      FlickrAPIKeys.PerPage: FlickrAPIValues.PerPage]
        switch self {
        case .getRecent(let page):
            params[FlickrAPIKeys.Method] = FlickrAPIValues.RecentMethod
            params[FlickrAPIKeys.Page] = "\(page)"
            return params
        case .search(let page, let text):
            params[FlickrAPIKeys.Method] = FlickrAPIValues.RecentMethod
            params[FlickrAPIKeys.SafeSearch] = FlickrAPIValues.SafeSearch
            params[FlickrAPIKeys.Page] = "\(page)"
            params[FlickrAPIKeys.Text] = text
            return params
        }
    }
    
    var path: String {
        return "services/rest"
    }
    
    
    
}
