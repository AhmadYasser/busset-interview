//
//  FavRoute.swift
//  Busset-Interview
//
//  Created by Ahmed Yasser on 29/08/2021.
//

import Foundation
import Alamofire
enum FavRoute: URLRequestConvertible {
    
    case addToFav(String)
    case removeFav(String)

    var method: HTTPMethod {
        return .get
    }
    
    var parameters: [String : Any]? {
        var params = [FlickrAPIKeys.APIKey: FlickrAPIValues.APIKey,
                      FlickrAPIKeys.Extras: FlickrAPIValues.MediumURL,
                      FlickrAPIKeys.ResponseFormat: FlickrAPIValues.ResponseFormat,
                      FlickrAPIKeys.DisableJSONCallback:FlickrAPIValues.DisableJSONCallback]

        switch self {
        case .addToFav(let photoID):
            params[FlickrAPIKeys.Method] = FlickrAPIValues.AddToFavMethod
            params[FlickrAPIKeys.PhotoID] = photoID
            return params
        case .removeFav(let photoID):
            params[FlickrAPIKeys.Method] = FlickrAPIValues.RemoveFavMethod
            params[FlickrAPIKeys.PhotoID] = photoID
            return params

        }
    }
    
    var path: String {
        return "services/rest"
    }



}
