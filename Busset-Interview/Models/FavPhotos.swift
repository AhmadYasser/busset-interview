//
//  FavPhotos.swift
//  Busset-Interview
//
//  Created by Ahmed Yasser on 29/08/2021.
//

import Foundation
import RealmSwift

@objcMembers class FavPhoto: Object {
    dynamic var id: String = ""
    dynamic var image: Data?
    dynamic var title: String = ""
    
    convenience init(id: String, image: Data, title: String) {
        self.init()
        self.id = id
        self.image = image
        self.title = title
    }
    
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
}
