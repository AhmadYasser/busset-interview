//
//  ResultsExt.swift
//  Busset-Interview
//
//  Created by Ahmed Yasser on 31/08/2021.
//

import RealmSwift
extension Results {
    func toArray() -> [Element] {
      return compactMap {
        $0
      }
    }
 }

