//
//  BehaviorRelayExt.swift
//  Busset-Interview
//
//  Created by Ahmed Yasser on 31/08/2021.
//

import RxCocoa

public extension BehaviorRelay where Element: RangeReplaceableCollection {


    func remove(at index: Element.Index) {
        var newValue = value
        newValue.remove(at: index)
        accept(newValue)
    }
}
