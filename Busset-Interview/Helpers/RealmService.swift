//
//  RealmService.swift
//  Busset-Interview
//
//  Created by Ahmed Yasser on 29/08/2021.
//

import Foundation
import RealmSwift

let sharedRealm = RealmServices.shared

class RealmServices{
    
    static let shared = RealmServices()
    var realm = try! Realm()
    private init() {}
    
    func add<T: Object>(_ x:T) {
        do {
            try realm.write {
                realm.add(x)
            }
        } catch {
            print(error)
        }
    }
    
    func update<T: Object>(_ x: T, with dictionary: [String: Any]) {
        do {
            try realm.write {
                for (key, value) in dictionary {
                    x.setValue(value, forKey: key)
                }
            }
        } catch {
            print(error)
        }
    }
    
    func read<T: Object>(_ model: T.Type) -> Results<T> {
        let objects = realm.objects(model)
        return objects
    }
    
    
    func delete<T: Object>(object: T.Type, predicate: NSPredicate, completion: (Bool) -> Void) {
        guard let object = realm.objects(object).filter(predicate).first else { return }
        
        do {
            try realm.write {
                realm.delete(object)
                completion(true)
            }
        } catch {
            completion(false)
            print(error)
        }
    }
    
    
    func object<T: Object>(object: T.Type, primaryKey: String) -> T? {
        return realm.object(ofType: object, forPrimaryKey: primaryKey)
    }
    
    func update<T: Object>(object: T) {
        do {
            try realm.write {
                realm.add(object, update: .modified)
            }

        } catch {
            print(error)
        }
    }
    
    
    
}
