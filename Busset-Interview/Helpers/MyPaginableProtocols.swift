//
//  MyPaginableProtocols.swift
//  Busset-Interview
//
//  Created by Ahmed Yasser on 30/08/2021.
//

import UIKit

protocol PaginablePresenter {
    var currentPage: Int {get}
    var lastPage: Int {get}
    var isFetching: Bool { get set}
    func paginate()

}


protocol PaginableView: UIScrollViewDelegate where Self: UIViewController {
    var scroll: UIScrollView {get}
    var isBottom: Bool {get}
    var paginablePresenter: PaginablePresenter {get}
    func didEndScrolling()
}

extension PaginableView {
    var isBottom: Bool {
      return (scroll.contentOffset.y + scroll.frame.height) >= scroll.contentSize.height
    }
        
    
    func didEndScrolling() {
        guard isBottom && !paginablePresenter.isFetching && paginablePresenter.lastPage > paginablePresenter.currentPage else {return}
        paginablePresenter.paginate()
    }

}
